# NakalaAPIv3Experiment

*Michael Nauge, Université de Poitiers*

*Consortium CAHIER, Consortium ADE, GT-Nakala-Interconsortium*

*Avril 2020*

## Chronique de mes premiers pas avec l'API v3 de Nakala

### Notes pour un éventuel lecteur


Ce document est un carnet de suivi personnel de mes premiers essais. 

Ce n'est pas un tutoriel, ni une documentation officielle. 

L'API est toujours en cours de développement (l'interface IHM basée sur cette API ayant pour date prévisionnelle de sortie plutôt la fin de cet été).
Ces notes sont réalisées sur la base d'une "API de test en cours de développement". 
Ce document ne sera pas rétro-compatible sur l’API finale. En effet, les tests de validations des métadonnées ne sont pas encore activés).

Ce carnet mériterait un travail de ré-écriture pour quelque chose de plus didactique. Mais j'attends d'avoir des *issues* à ce sujet sur le *GIT* pour ça ;-)


#### Rappel

La nouvelle version de Nakala va grandement faciliter le dépôt massif des ressources avec de nouvelles interfaces utilisateurs et le chargement de tableurs de métadonnées. Ces nouveaux systèmes de dépôt vont s'appuyer sur des points d'accès logiciels (API) pour fonctionner.
Il s'agit ici de tester ces points d'accès logiciels (API). 

Il n'y a rien d'ergonomique ici. Ce sont des tests plutôt adressés à un public familier de la programmation web et des API REST.


## Objectifs

- Créer une collection permettant d'agréger des datas
- Créer une data contenant plusieurs fichiers
- Créer une seconde data contenant plusieurs fichiers
- Aggréger les datas à la collection


## Consulter la chronique
[Lire en pdf](testAPIv3.pdf)


*note* 

le pdf a été généré avec la commande : pandoc testAPIv3.ipynb -o testAPIv3.pdf